package umb.arquetipo.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import umb.arquetipo.micro.user.Users;

public class UsersVerticle extends AbstractVerticle {
    Users service;
    private MessageConsumer<JsonObject> binder;

    @Override
    public void start() {
        this.service = Users.create(vertx, config());
        this.binder = new ServiceBinder(vertx)
                .setAddress("user-service")
                .register(Users.class, this.service);
    }

    @Override
    public void stop() {
        this.binder.unregister();
    }
}
