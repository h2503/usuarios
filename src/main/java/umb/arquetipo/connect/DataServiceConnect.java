package umb.arquetipo.connect;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;

public interface DataServiceConnect {
    static PgPool open(Vertx vertx, JsonObject configuration, String name) {
        return new DataServiceConnectImpl().open(vertx, configuration, name);
    }
}
