package umb.arquetipo.connect;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DataServiceConnectImpl {

    public PgPool open(Vertx vertx, JsonObject configuration, String name) {

        Map<String, String> properties = new HashMap<>();
        properties.put("application_name", name);

        return PgPool.pool(vertx, new PgConnectOptions()
                .setPort(configuration.getInteger("PG_DB_PORT"))
                .setHost(configuration.getString("PG_DB_HOST"))
                .setDatabase(configuration.getString("PG_DB_NAME"))
                .setUser(configuration.getString("PG_DB_USER"))
                .setPassword(configuration.getString("PG_DB_PASS"))
                .setProperties(properties), new PoolOptions().setMaxSize(30).setIdleTimeoutUnit(TimeUnit.SECONDS).setIdleTimeout(1));
    }
}
