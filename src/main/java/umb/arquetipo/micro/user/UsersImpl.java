package umb.arquetipo.micro.user;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Tuple;
import org.apache.log4j.Logger;
import umb.arquetipo.connect.DataServiceConnect;
import umb.arquetipo.micro.dto.UsersDTO;

import java.util.ArrayList;
import java.util.List;

public class UsersImpl implements Users {
    private static final Logger logger = Logger.getLogger(UsersImpl.class);
    PgPool pgClient;

    UsersImpl(Vertx vertx, JsonObject configuration) {
        this.pgClient = DataServiceConnect.open(vertx, configuration, "users");
    }

    @Override
    public Future<Integer> nuevoUsuario(JsonObject arguments) {
        return this.pgClient.preparedQuery("INSERT INTO public.usuarios(nombre, primer_apellido, segundo_apellido, correo, contrasenia, vigente, num_perfiles)VALUES ( $1, $2, $3, $4, $5, $6, $7) returning id_usuario")
                .execute(Tuple.of(
                        arguments.getJsonObject("usuario").getString("nombre"),
                        arguments.getJsonObject("usuario").getString("primerApellido"),
                        arguments.getJsonObject("usuario").getString("segundoApellido"),
                        arguments.getJsonObject("usuario").getString("correo"),
                        arguments.getJsonObject("usuario").getString("contrasenia"),
                        arguments.getJsonObject("usuario").getBoolean("vigente"),
                        arguments.getJsonObject("usuario").getInteger("perfiles")
                ))
                .map(dataDb -> {
                    dataDb.forEach(row -> arguments.put("idUsuario", row.toJson().getInteger("id_usuario")));
                    return arguments.getInteger("idUsuario");
                });
    }

    @Override
    public Future<UsersDTO> usuarioPorId(JsonObject arguments) {
        return this.pgClient.preparedQuery("select id_usuario as \"idUsuario\",nombre, primer_apellido as \"primerApellido\", segundo_apellido as \"segundoApellido\",correo,contrasenia,vigente,num_perfiles as \"perfiles\" from usuarios where id_usuario = $1")
                .execute(Tuple.of(arguments.getInteger("idUsuario")))
                .map(dataDb -> {
                    dataDb.forEach(row -> arguments.put("usuario", row.toJson()));
                    return new UsersDTO(arguments.getJsonObject("usuario"));
                });
    }
    @Override
    public Future<List<UsersDTO>> usuarioPorApellido(JsonObject arguments) {
        return this.pgClient.preparedQuery("select id_usuario as \"idUsuario\",nombre, primer_apellido as \"primerApellido\", segundo_apellido as \"segundoApellido\",correo,contrasenia,vigente,num_perfiles as \"perfiles\" from usuarios where primer_apellido = $1")
                .execute(Tuple.of(arguments.getString("primerApellido")))
                .map(dataDb -> {
                    List<UsersDTO> usuarios = new ArrayList<>();
                    dataDb.forEach(row -> usuarios.add(new UsersDTO(row.toJson())));
                    return usuarios;
                });
    }

    @Override
    public Future<List<UsersDTO>> usuarioPorNombre(JsonObject arguments) {
        return this.pgClient.preparedQuery("select id_usuario as \"idUsuario\",nombre, primer_apellido as \"primerApellido\", segundo_apellido as \"segundoApellido\",correo,contrasenia,vigente,num_perfiles as \"perfiles\" from usuarios where nombre = $1")
                .execute(Tuple.of(arguments.getString("nombre")))
                .map(dataDb -> {
                    List<UsersDTO> usuarios = new ArrayList<>();
                    dataDb.forEach(row -> usuarios.add(new UsersDTO(row.toJson())));
                    return usuarios;
                });
    }

    public Future<List<UsersDTO>> usuarios() {
        return this.pgClient.preparedQuery("select id_usuario as \"idUsuario\",nombre, primer_apellido as \"primerApellido\", segundo_apellido as \"segundoApellido\",correo,contrasenia,vigente,num_perfiles as \"perfiles\" from usuarios")
                .execute()
                .map(dataDb -> {
                    List<UsersDTO> usuarios = new ArrayList<>();
                    dataDb.forEach(row -> usuarios.add(new UsersDTO(row.toJson())));
                    return usuarios;
                });
    }

}
