package umb.arquetipo.micro.user;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import umb.arquetipo.micro.dto.UsersDTO;

import java.util.List;

@ProxyGen
@VertxGen
public interface Users {
    static Users create(Vertx vertx, JsonObject configuration) {
        return new UsersImpl(vertx, configuration);
    }

    static Users createProxy(Vertx vertx, String address) {
        return new UsersVertxEBProxy(vertx, address);
    }

    Future<Integer> nuevoUsuario(JsonObject arguments);

    Future<UsersDTO> usuarioPorId(JsonObject arguments);

    Future<List<UsersDTO>> usuarios ();

    Future<List<UsersDTO>> usuarioPorApellido(JsonObject arguments);
    Future<List<UsersDTO>> usuarioPorNombre(JsonObject arguments);

}